<?php

namespace AbuserLog;

require_once 'vendor/autoload.php';
require_once 'Classes/BasicAuthentication.php';
require_once 'Classes/abuseAddress.php';
require_once 'Classes/Database.php';
require_once 'Classes/Key.php';

use Luracast\Restler\Restler;
use Monolog\Logger;
use Monolog\Handler\SyslogHandler;
use Monolog\Formatter\LineFormatter;

try {

    $logger = new Logger('AbuserLogger');
    $syslog = new SyslogHandler('AbuserLog', 'local6');
    $formatter = new LineFormatter();
    $syslog->setFormatter($formatter);
    $logger->pushHandler($syslog);
    $restler = new Restler();

    //this creates resources.json at API Root
    $restler->addAPIClass('Luracast\\Restler\\Resources');
} catch (\Exception $e) {
    header("HTTP/1.1 500 Internal Server Error", true, 500);
    $logger->addError("Runtime error: ".$e->getMessage());
    exit(0);
}
try {
    $restler->addAPIClass('AbuserLog\\Classes\\abuseAddress', '');
    $restler->addAPIClass('AbuserLog\\Classes\\Key', '');
    $restler->addAuthenticationClass('AbuserLog\\Classes\\BasicAuthentication');
    $restler->handle();
} catch (\Exception $e) {
    header("HTTP/1.1 500 Internal Server Error", true, 500);
    $logger->addError("Runtime error: ".$e->getMessage());
    exit(0);
}

