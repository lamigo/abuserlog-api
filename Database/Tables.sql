CREATE TABLE keys (
id SERIAL PRIMARY KEY,
source inet,
apikey varchar(64)
)

CREATE OR REPLACE FUNCTION getkey(address inet) RETURNS text AS $$
DECLARE
  myrow RECORD;
  mykey TEXT;
BEGIN
    SELECT keys.apikey INTO myrow FROM keys WHERE keys.source = getkey.address;
    IF myrow.apikey IS NULL THEN
      SELECT encode((getkey.address::text||to_char(now(),'YYYY-mm-dd hh:mm:ss'))::bytea,'base64'::text) INTO mykey;
      INSERT INTO keys(source,apikey) VALUES (
      getkey.address,
      mykey
      );
    ELSE
      mykey:=myrow.apikey;
    END IF;
    RETURN mykey;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkkey(address inet, apikey varchar) RETURNS boolean AS $$
DECLARE
  myrow RECORD;
BEGIN
    SELECT keys.apikey INTO myrow FROM keys WHERE keys.source = checkkey.address AND keys.apikey = checkkey.apikey;
    IF myrow.apikey IS NULL THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
END;
$$ LANGUAGE plpgsql;



CREATE TABLE abuse_input (
entryid SERIAL PRIMARY KEY,
submit_date TIMESTAMP DEFAULT NOW(),
address inet NOT NULL,
action varchar(64),
source inet
);

CREATE TABLE abuse_process {
offenderid SERIAL PRIMARY KEY,
address inet NOT NULL,
creation_date TIMESTAMP,
update_date TIMESTAMP,
report_count INTEGER,
actions hstore
};

CREATE TABLE address_data (
id SERIAL PRIMARY KEY,
address inet NOT NULL,
creation_date TIMESTAMP,
update_date TIMESTAMP,
country VARCHAR(128),
country_code VARCHAR(3),
country_code_check VARCHAR(3),
city VARCHAR(128),
continent VARCHAR(32),
latitude FLOAT,
longitude FLOAT,
time_zone VARCHAR(32),
postal_code VARCHAR(32),
org VARCHAR(256),
asn VARCHAR(32),
subdivision VARCHAR(128),
subdivision2 VARCHAR(128)
)


CREATE OR REPLACE FUNCTION abusersmap() RETURNS text AS '
library(rworldmap)
sqlINPUT <- paste("select country_code, count(*) from address_data group by country_code order by count(*) desc;");
strINPUT <- pg.spi.exec (sqlINPUT);

sPDF <- joinCountryData2Map(strINPUT,
, joinCode = "ISO2"
, nameJoinColumn = "country_code"
, verbose = TRUE);
mapDevice(''png'',filename=''/tmp/graph1.png'',width = 1024);
mapCountryData(sPDF,nameColumnToPlot=''count'',catMethod=''pretty'',
        oceanCol = ''lightblue'',
        missingCountryCol= ''wheat'', addLegend=F);
dev.off();
print (''DONE'');
' LANGUAGE plr;