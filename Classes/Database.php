<?php
/**
 * Created by PhpStorm.
 * User: lamigo
 * Date: 29/04/2018
 * Time: 23:14
 * PHP: 5.6
 */

namespace AbuserLog\Classes;

/**
 * Class Database
 *
 * @package AbuserLog\Classes
 * @author  Luis Amigo <lamigo@praderas.org>
 * @license MIT LIcense
 * @link    www.praderas.org
 */
class Database
{
    public $dbHostname;
    public $dbPort;
    public $dbDatabase;
    public $dbUser;
    public $dbPassword;
    public $dbConn;

    /**
     * Database constructor.
     */
    public function __construct()
    {
        $this->dbHostname='localhost';
        $this->dbPort='5432';
        $this->dbDatabase='abuseip';
        $this->dbUser='abuseip';
        $this->dbPassword='';
        $connString = sprintf(
            "pgsql:host=%s;port=%d;dbname=%s;user=%s;password=%s",
            $this->dbHostname,
            $this->dbPort,
            $this->dbDatabase,
            $this->dbUser,
            $this->dbPassword
        );
        try {
            $this->dbConn = new \PDO($connString);
            $this->dbConn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            return false;
        }
        return true;
    }

    /**
     * Inserts record in database
     *
     * @param array $params request params
     *
     * @return bool
     */
    public function addAdress($params)
    {
        $query = "INSERT INTO abuse_input VALUES(".
            "default,".
            "now(),'".
            $params['address']."','".
            $params['action']."','".
            $_SERVER['REMOTE_ADDR']."')";
        $statement = $this->dbConn->prepare($query);
        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return false;
        }
        return true;
    }

    /**
     * Checks the params of the call
     *
     * @param array $params parameters of the call
     *
     * @return bool
     */
    public function checkParams($params)
    {
        if (!isset($params['address'])
            || !isset($params['action'])
        ) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the API key linked to the IP
     *
     * @return bool|mixed
     */
    public function getKey()
    {
        $query = "SELECT getkey('".$_SERVER['REMOTE_ADDR']."'::inet)";
        $statement = $this->dbConn->prepare($query);
        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return false;
        }
        return $statement->fetchColumn(0);
    }

    /**
     * Checks the API key linked to the IP
     *
     * @param string $token API token sent by client
     *
     * @return bool|mixed
     */
    public function checkKey($token)
    {
        $query = "SELECT checkkey('".$_SERVER['REMOTE_ADDR']."'::inet, '".$token."'::varchar)";
        $statement = $this->dbConn->prepare($query);
        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return false;
        }
        $result=$statement->fetchColumn(0);
        if ($result=='t') {
            return true;
        }
        return false;
    }
}