<?php
/**
 * Created by PhpStorm.
 * User: lamigo
 * Date: 05/05/2018
 * Time: 10:04
 */

namespace AbuserLog\Classes;


class Key
{
    /**
     * @url GET key/request/
     *
     * @return string
     */
    public function get() {
        $database = new Database();
        $key=$database->getKey();
        if (!$key) {
            header("HTTP/1.1 403 Forbidden", true, 403);
            return "Forbidden";
        }
        return $key;
    }
}