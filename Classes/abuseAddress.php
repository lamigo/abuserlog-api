<?php
/**
 * Created by PhpStorm.
 * User: lamigo
 * Date: 28/04/2018
 * Time: 22:41
 *
 *
 */

namespace AbuserLog\Classes;


class abuseAddress
{

    /**
     * Adds a new IP to the the database
     *
     * @url    POST add
     * @access protected
     * @throws \Exception
     * @return string
     */
    protected function post($request_data=null)
    {
        $database = new Database();
        if ($database->checkParams($request_data)) {
            if (!$database->addAdress($request_data)) {
                header("HTTP/1.1 500 Internal Server Error", true, 500);
                return "wrong parameters";
            }
        } else {
            header("HTTP/1.1 500 Internal Server Error", true, 500);
            return "wrong parameters";
        }
        return "data added";
    }
}