<?php

namespace AbuserLog\Classes;

use Luracast\Restler\iAuthenticate;

/**
 * Class BasicAuthentication
 *
 * @category Utils
 * @package  AbuserLog\Classes
 * @author   Luis Amigo <lamigo@praderas.org>
 * @license  MIT LIcense
 * @link     www.praderas.org
 */
class BasicAuthentication implements iAuthenticate
{
    const REALM = 'Restricted API';

    /**
     * Basic authentication for API class
     *
     * @return bool
     * @throws \Exception
     */
    function __isAllowed()
    {
        // It checks if the user and password have been entered.
        // Otherwise, he will re-send us to the apache form.
        // In case the password is erroneous it will redirect us to the login
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            $user = $_SERVER['PHP_AUTH_USER'];
            $pass = $_SERVER['PHP_AUTH_PW'];
            // right now we only request a username and a password, it is not needed to be right ones
            return true;
        } else {
            $bearer = $this->getBearerToken();
            if ($bearer) {
                $database = new Database();
                $res = $database->checkKey($bearer);
                if ($res) {
                    return true;
                } else {
                    return false;
                }
            } else {
                header('WWW-Authenticate: Basic realm="' . self::REALM . '" ');
                header("HTTP/1.1 401 Unauthorized", true, 401);
                $message = "Authentication required";
                return false;
            }
        }
    }

    /**
     * Returns Authenticate String for Browser
     *
     * @return string
     */
    public function __getWWWAuthenticateString()
    {
        return 'Basic';
    }

    /**
     * Get header Authorization
     *
     * @return mixed
     */
    function getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } else if (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            /*
             * Server-side fix for bug in old Android versions
             * (a nice side-effect of this fix means
             *  we don't care about capitalization for Authorization)
             */
            $requestHeaders = array_combine(
                array_map(
                    'ucwords',
                    array_keys($requestHeaders)
                ),
                array_values($requestHeaders)
            );
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }
    /**
     * Get access token from header
     *
     * @return mixed
     */
    function getBearerToken()
    {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return false;
    }
}
