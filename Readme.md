# Abuserlog API

### Summary

The main objective of the project is having a tool like badips but usable on your own organization.

### Architecture

An API entrypoint to log all IP bans on a fail2ban action
A periodic process to reduce entries for further usage
A process to take data logs of reference
A web to check information
An API entrypoint to download IP List for banlist

### Features

Log IP Bans on your own server from all servers on your organization.
Use net reduction to have a minimal set of Nets to ban
Use as an entrypoint to integrate with logs

### Usage

> **GET** /key retrieves API key for authorized 
>
> **POST** params  {"address", "action", "key"}