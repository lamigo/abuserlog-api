<?php
/**
 * Created by PhpStorm.
 * User: lamigo
 * Date: 01/05/2018
 * Time: 18:58
 */

$dbHostname='localhost';
$dbPort='5432';
$dbDatabase='abuseip';
$dbUser='abuseip';
$dbPassword='';
$connString = sprintf(
    "pgsql:host=%s;port=%d;dbname=%s;user=%s;password=%s",
    $dbHostname,
    $dbPort,
    $dbDatabase,
    $dbUser,
    $dbPassword
);
try {
    $dbConn = new \PDO($connString);
    $dbConn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
} catch (\PDOException $e) {
    return "Exception: $e->getMessage()";
}

$stmt = $dbConn->prepare("select distinct(address) from abuse_input");
$stmt->execute();

while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
    $check = $dbConn->prepare(
        "select count(address) FROM address_data WHERE address='".$row['address']."'"
    );
    $check->execute();
    if (($check->fetch(PDO::FETCH_COLUMN)) > 0) {
        continue;
    }
    $data=checkip($row['address']);
    $insert = $dbConn->prepare(
        "INSERT INTO address_data(id,address,creation_date,
        update_date,country,country_code,country_code_check,city,continent,latitude,
        longitude,time_zone,postal_code,org,asn,subdivision,subdivision2) VALUES (
        default,'".
        $row['address']."',".
        "now(),now(),'".
        $data['country']."','".
        $data['country_code']."','".
        $data['country_code_check']."','".
        $data['city']."','".
        $data['continent']."','".
        $data['latitude']."','".
        $data['longitude']."','".
        $data['time_zone']."','".
        $data['postal_code']."','".
        $data['org']."','".
        $data['asn']."','".
        $data['subdivision']."','".
        $data['subdivision2']."')"
    );
    $insert->execute();
}
exit(0);

/**
 * @param $address
 * @return bool|mixed|string
 */
function checkip($address)
{
    $res = file_get_contents('https://www.iplocate.io/api/lookup/'.$address);
    $res = json_decode($res, true);
    list($d1, $d2, $d3, $d4) = explode('.', $address);
    $ip_address = gethostbyname("$d4.$d3.$d2.$d1.rbloc.dayanadns.com");
    list($d1, $d2, $d3, $d4) = explode('.', $ip_address);
    $res['country_code_check'] = chr($d3).chr($d4);
    return $res;
}