CREATE TABLE  rsyslog (
id SERIAL PRIMARY KEY,
message TEXT,
timereported TIMESTAMP,
source INET,
tag VARCHAR(255),
program VARCHAR(255),
facility VARCHAR(255),
severity VARCHAR(255));
